Q=@
RM=rm -rfv
DIR_NUTTX=nuttx
DIR_APPS=apps
DIR_ROMFS=$(DIR_NUTTX)/include/arch/board
APPS_CUSTOM=apps-custom
SIMULATOR=simulator
LM3S_CUSTOM=lm3s6965-ek-custom
ROMFSIMG=romfs_img
ROMFSIMG_HEADER=nsh_romfsimg.h


.PHONY: all
all:

.PHONY: prepare-sim
prepare-sim: _prepare
	$(Q) ./$(DIR_NUTTX)/tools/configure.sh -l $(SIMULATOR)/a

.PHONY: prepare-lm3s
prepare-lm3s: _prepare
	$(Q) ./$(DIR_NUTTX)/tools/configure.sh -l $(LM3S_CUSTOM)/a

.PHONY: _prepare
_prepare: | $(DIR_NUTTX)/configs/$(SIMULATOR)
_prepare: | $(DIR_NUTTX)/configs/$(LM3S_CUSTOM)
_prepare: | $(DIR_APPS)/$(APPS_CUSTOM)

.PHONY: clean
clean: current-clean nuttx-clean

.PHONY: current-clean
current-clean:
	$(RM) \
		$(DIR_ROMFS)/$(ROMFSIMG) \
		$(DIR_ROMFS)/$(ROMFSIMG_HEADER)

.PHONY: nuttx-clean
nuttx-clean:
	$(Q) $(MAKE) -C $(DIR_NUTTX) clean

.PHONY: distclean
distclean: clean rm-symlinks
	$(Q) $(MAKE) -C $(DIR_NUTTX) $@

.PHONY: rm-symlinks
rm-symlinks:
	$(Q) find $(DIR_NUTTX) -type l -delete
	$(Q) find $(DIR_APPS) -type l -delete
	$(Q) find $(SIMULATOR) -type l -delete
	$(Q) find $(LM3S_CUSTOM) -type l -delete

all: $(DIR_NUTTX)/nuttx

$(DIR_ROMFS)%:
	$(Q) $(MAKE) -C $(DIR_NUTTX) context

$(DIR_ROMFS)/$(ROMFSIMG): $(DIR_ROMFS)/etc
	@ echo "GENROMFS: $@"
	$(Q) genromfs -f $@ -d $< -V SimEtcVol

$(DIR_ROMFS)/$(ROMFSIMG_HEADER): $(DIR_ROMFS)/$(ROMFSIMG)
	@ echo "XXD: $@"
	$(Q) hexdump -v -e '8/1 "0x%02x, "' -e '"\n"' $< | awk \
		'BEGIN{print "const unsigned char romfs_img[] = {"} \
		{print "  "$$0} \
		END{print "};\n\nconst unsigned int romfs_img_len = " NR*8 ";"}' \
		> $@

$(DIR_NUTTX)/nuttx: $(DIR_ROMFS)/nsh_romfsimg.h
	$(Q) $(MAKE) -C $(DIR_NUTTX) $(@F)

$(DIR_NUTTX)/configs/$(SIMULATOR): | $(SIMULATOR)
	$(Q) ln -s ../../$(SIMULATOR) $@

$(DIR_NUTTX)/configs/$(LM3S_CUSTOM): | $(LM3S_CUSTOM)
	$(Q) ln -s ../../$(LM3S_CUSTOM) $@

$(DIR_APPS)/$(APPS_CUSTOM): | $(APPS_CUSTOM)
	$(Q) ln -s ../$(APPS_CUSTOM) $@

%:
	$(Q) $(MAKE) -C $(DIR_NUTTX) $@
