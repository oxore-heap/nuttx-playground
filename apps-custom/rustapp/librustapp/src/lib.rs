// std and main are not available for bare metal software

#![no_std]

extern "C" {
    fn puts(s: *const i8) -> i32;
}

#[no_mangle]
fn rustapp_main() {
    unsafe {
        puts("Hello, world! From Rust with unsafes...\0".as_ptr() as *const i8);
    }
}
