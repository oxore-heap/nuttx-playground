#include "nuttx/config.h"

extern int CONFIG_USER_ENTRYPOINT(int argc, char *const argv[]);

int main(int argc, char *const argv[]) {
    CONFIG_USER_ENTRYPOINT(argc, argv);
}
