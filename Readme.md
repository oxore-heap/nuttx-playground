# Simulator

1. Configure the build

```
make prepare-sim
```

2. Enable `CONFIG_SIM_M32`. Despite this option is enabled in
   `simulator/defconfig`, it is not set by nuttx configuration script for
   unknown reason.

2.1. Use `menuconfig` or `nconfig` or anything else. I actually prefer
`nconfig`:

```
make nconfig
```

2.3 Go to `System Type --->` and check `Build 32-bit simulation on 64-bit
machine` with space or enter. Then exit (F9).

3. Build the simulator

```
make
```

4. Run the simulator

```
./nuttx/nuttx
```

or better with gdb in order to not mess up your terminal in case of crash:

```
gdb ./nuttx/nuttx
run
```

# Custom `lm3s6965-ek` config

1. Configure the build

```
make prepare-lm3s
```

3. Build `lm3s6965-ek` target

```
make
```

4. Run QEMU

```
./scripts/qemu-lm3s.sh
```


# Micropython

To use micropython with NuttX on various platform it needs to be built by
custom Makefile, which depends on NuttX makefile system. Every app is compiled
by `COMPILE` function, defined somewhere in NuttX's makefiles, so the
micropython makefile must be somehow included by it and used properly. Writing
and maintaining makefiles for this looks like astonishing amount of work.
Without that micropython's makefiles will not receive proper include
directories locations. Though you can successfully build it with `x86_64` NuttX
simulation.

# Rust code

This repo contains example of using rust programs in NuttX. In order to compile
rust code for NuttX simulator or any platform you will need a rust compilation
facilities for a specific target.

Prerequisites:

- Rust build environment (go google how to start with rust if you don't know)
- Nightly enabled, since arm targets are nightly only.
- Specific build target.

For simulator, as it is 32-bit only:

```
rustup target add i686-unknown-linux-gnu
```

For lm3s6965-ek target, as it has armv7 (thumb2) ISA:

```
rustup target add thumbv7m-none-eabi
```

# Networking

To allow NuttX creating TAP device and actually be able to control raw L2
sockets, network administration capabilities should be set on the executable:

```
sudo setcap cap_net_admin=ep ./nuttx/nuttx
```

After that setup the `nuttx0` bridge network device and iptables forwarding:

```
sudo ./scripts/bridge-up.sh
sudo ./scripts/forwarding.sh
```

Now you can run Nuttx with network enabled. Make sure it has address compiled
in, that belongs to network `192.168.168.1/24`. It will create some tap device
and connect it to the `nuttx0` bridge device.

```
./nuttx/nuttx
```

Remove the bridge when you finish your work. It is always optional.

```
sudo ./scripts/bridge-down.sh
```

# TODO

- [ ] `/dev/ttyS1`, `/dev/ttyS2`, `/dev/ttyS3` and so on point to real Linux
  ttys specified in config by user.
- [x] Networking with TAP and bridge.
- [ ] `/dev/led1`, `/dev/led2` and so on are exported somehow to Linux... as
  pty's.
- [x] Custom `lm3s6965-ek` config.
- [ ] Micropython on `lm3s6965-ek`.
- [x] Rust on `lm3s6965-ek`.
- [x] Rust on simulator.
