#!/bin/sh

DEVNAME="nuttx0"
NET="192.168.168.1/24"

sysctl net.ipv4.ip_forward=1

ip link add name $DEVNAME type bridge
ip link set $DEVNAME up
ip addr add $NET dev $DEVNAME
