#!/bin/sh

IPT=$(which iptables)
IF_INT="nuttx0"
IF_EXT="wlp3s0"
NET_INT="192.168.168.0/24" # Bridge address
IP_EXT="192.168.0.104" # Local machine address

# Cleanup ifconfig rules
$IPT -F
$IPT -X
$IPT -Z
# Cleanup nat table
$IPT -t nat -F
$IPT -t nat -X
$IPT -t nat -Z
# Cleanup postrouting also
$IPT -t nat -F POSTROUTING

# Allow internal and external forwarding
$IPT -A FORWARD -i $IF_EXT -o $IF_INT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
$IPT -A FORWARD -i $IF_INT -o $IF_EXT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
# NAT itself
$IPT -t nat -A POSTROUTING -s $NET_INT -o $IF_EXT -j SNAT --to-source $IP_EXT
